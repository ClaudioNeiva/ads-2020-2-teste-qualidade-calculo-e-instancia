package br.ucsal.testequalidade.unitarioscommockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.testequalidade.CalculoEHelper;
import br.ucsal.testequalidade.FatorialHelper;

public class CalculoEHelperTest {

	private static FatorialHelper fatorialHelperMock;

	private static CalculoEHelper calculoEHelper;

	@BeforeAll
	public static void setup() {
		fatorialHelperMock = Mockito.mock(FatorialHelper.class);
		
		Mockito.when(fatorialHelperMock.calcularFatorial(0)).thenReturn(1L);
		Mockito.when(fatorialHelperMock.calcularFatorial(1)).thenReturn(1L);
		Mockito.when(fatorialHelperMock.calcularFatorial(2)).thenReturn(2L);
		
		calculoEHelper = new CalculoEHelper(fatorialHelperMock);
	}

	@Test
	public void calcularE2() {
		Integer n = 2;
		Double eEsperado = 2.5;
		Double eAtual = calculoEHelper.calcularE(n);
		assertEquals(eEsperado, eAtual);
	}

}
